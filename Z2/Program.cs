﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD2
{
    class Program
    {
        static void Main(string[] args)
        {
            Box box = new Box();

            box.AddProduct(new Product("Nintendo Switch", 3000.0));
            box.AddProduct(new Product("Nintendo Switch Lite", 2000.0));
            box.AddProduct(new Product("Animal Crossing", 500.0));
            box.AddProduct(new Product("Zelda, Breath of the Wild", 500.0));
            box.AddProduct(new Product("Skyrim", 500.0));

            Iterator iterator = (Iterator)box.GetIterator();

            Console.WriteLine("Items in box:");
            for (Product product = iterator.First(); iterator.IsDone == false; product = iterator.Next())
            {
                product.Show();
            }
            Console.WriteLine("Total cost of items in box: " + box.Cost());

            Console.ReadKey();
        }
    }
}
