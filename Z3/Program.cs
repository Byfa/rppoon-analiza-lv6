﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD3
{
    class Program
    {
        static void Main(string[] args)
        {
            ToDoItem toDoItem = new ToDoItem("RPPOON:", "Promjeniti repozitorij u javno", new DateTime(2020, 5, 14, 23, 59, 59));

            CareTaker careTaker = new CareTaker();
            careTaker.SetLastState(toDoItem.StoreState());

            toDoItem.Rename("PREDAT IZVJESCE:");
            toDoItem.ChangeTask("Napraviti KM LV.");
            toDoItem.ChangeTimeDue(new DateTime(2020, 5, 17, 23, 59, 59));
            careTaker.SetLastState(toDoItem.StoreState());

            toDoItem.Rename("PRIPREMIT SE ZA LV:");
            toDoItem.ChangeTask("Pogledati TI LV predlozak.");
            toDoItem.ChangeTimeDue(new DateTime(2020, 5, 14, 23, 59, 59));
            
            Console.WriteLine("3rd state: \n" + toDoItem.ToString() + "\n");

            toDoItem.RestoreState(careTaker.GetLastState());
            Console.WriteLine("2nd state: \n" + toDoItem.ToString() + "\n");

            toDoItem.RestoreState(careTaker.GetLastState());
            Console.WriteLine("1st state: \n" + toDoItem.ToString());

            Console.ReadKey();
        }
    }
}
