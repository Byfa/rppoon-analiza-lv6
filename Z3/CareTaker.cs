﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD3
{
    class CareTaker
    {
        public Stack<Memento> PreviousStates = new Stack<Memento>();
        public void SetLastState(Memento memento)
        {
            this.PreviousStates.Push(memento);
        }
        public Memento GetLastState()
        {
            return this.PreviousStates.Pop();
        }
    }
}
