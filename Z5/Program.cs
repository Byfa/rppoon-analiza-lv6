﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD5
{
    class Program
    {
        static void Main(string[] args)
        {
            AbstractLogger logger = new ConsoleLogger(MessageType.ALL);
            FileLogger fileLogger = new FileLogger(MessageType.ERROR | MessageType.WARNING, "logFile.txt");
            logger.SetNextLogger(fileLogger);
            logger.Log("LOG ERROR", MessageType.ERROR);
            logger.Log("LOG INFO", MessageType.INFO);
            logger.Log("LOG WARNING", MessageType.WARNING);
            //logger ce upisati u file samo error i warning
            Console.ReadKey();
        }
    }
}
