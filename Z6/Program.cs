﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD6
{
    class Program
    {
        static void Main(string[] args)
        {
            StringChecker stringChecker = new StringDigitChecker();
            stringChecker.SetNext(new StringLengthChecker());
            stringChecker.SetNext(new StringLowerCaseChecker());
            stringChecker.SetNext(new StringUpperCaseChecker());

            Console.WriteLine("First string:");
            if (stringChecker.Check("IWasBornToFail"))
            {
                Console.WriteLine("String has passed the test.");
            }
            else Console.WriteLine("String has failed the test.");

            Console.WriteLine("\nSecond string:");
            if (stringChecker.Check("IWasBornToPass1!!"))
            {
                Console.WriteLine("String has passed the test.");
            }
            else Console.WriteLine("String has failed the test.");

            Console.ReadKey();
        }
    }
}
