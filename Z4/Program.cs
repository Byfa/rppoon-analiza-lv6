﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD4
{
    class Program
    {
        static void Main(string[] args)
        {
            BankAccount bankAccount = new BankAccount("David Nadj", " <.< ", 5000);

            Console.WriteLine("1st state: \n" + bankAccount.ToString());
            Memento bankAccountSave = bankAccount.StoreState();

            bankAccount.ChangeOwnerAddress(" ? . ? ");
            bankAccount.UpdateBalance(-4500);
            Console.WriteLine("\n2nd state: \n" + bankAccount.ToString() + "\n");

            bankAccount.RestoreState(bankAccountSave);
            Console.WriteLine("1st state(restored): \n" + bankAccount.ToString());

            Console.ReadKey();
        }
    }
}
