﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV6
{
    class Program
    {
        static void Main(string[] args)
        {
            Notebook notebook = new Notebook();

            notebook.AddNote(new Note("Note numero uno:", "Somebody come get herrrrrr, she's dancing ....."));
            notebook.AddNote(new Note("Note numero dos:", "Oikawa did it"));
            notebook.AddNote(new Note("Note numero tres:", "Akaashi!!!! Oya oya oya :D"));

            Iterator iterator = (Iterator)notebook.GetIterator();
            
            while (!(iterator.IsDone))
            {
                iterator.Current.Show();
                iterator.Next();
            }

            Console.ReadKey();
        }
    }
}
