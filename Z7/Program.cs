﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD7
{
    class Program
    {
        static void Main(string[] args)
        {
            StringChecker stringChecker = new StringDigitChecker();
            stringChecker.SetNext(new StringLengthChecker());
            stringChecker.SetNext(new StringLowerCaseChecker());
            stringChecker.SetNext(new StringUpperCaseChecker());

            StringChecker passwordChecker = new PasswordValidator(stringChecker);

            Console.WriteLine("First password:");
            if (passwordChecker.Check("AStrongPasswordIsAGoodPassword"))
            {
                Console.WriteLine("Password has passed the check.");
            }
            else
            {
                Console.WriteLine("Password has failed the check.");
            }

            Console.WriteLine("\nSecond password:");
            if (passwordChecker.Check("AStrongPasswordIsAGoodPassword1!!"))
            {
                Console.WriteLine("Password has passed the check.");
            }
            else
            {
                Console.WriteLine("Password has failed the check.");
            }

            Console.ReadKey();
        }
    }
}
