﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD7
{
    class PasswordValidator : StringChecker
    {
        private StringChecker firstStringChecker;
        private StringChecker lastStringChecker;
        public PasswordValidator(StringChecker firstStringChecker)
        {
            this.firstStringChecker = firstStringChecker;
        }

        public void addNewChecker(StringChecker newChecker)
        {
            this.lastStringChecker = newChecker;
        }

        protected override bool PerformCheck(string stringToCheck)
        {
            if (firstStringChecker.Check(stringToCheck))
            {
                return true;
            }
            return false;
        }
    }
}

